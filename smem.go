//go:build linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package smem

import (
	"sort"

	"gopkg.org/proc"
)

func New() (Processes, error) {
	ps, err := proc.All()
	if err != nil {
		return nil, err
	}

	processes := make(Processes, 0)

	for _, p := range ps {
		c, err := p.CmdLine()
		if err != nil {
			continue
		}

		s, err := p.SMaps()
		if err != nil {
			continue
		}

		processes = append(processes, Process{
			PID:     p.PID,
			User:    p.User.Username,
			Command: c,
			Swap:    s.Swap(),
			VSS:     s.Size(),
			USS:     s.USS(),
			PSS:     s.PSS(),
			RSS:     s.RSS(),
		})
	}

	sort.Sort(processes)

	return processes, nil
}

type Processes []Process

func (p Processes) Len() int           { return len(p) }
func (p Processes) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p Processes) Less(i, j int) bool { return p[i].PSS < p[j].PSS }

type Process struct {
	PID     int
	User    string
	Command string
	Swap    uint64
	VSS     uint64
	USS     uint64
	PSS     uint64
	RSS     uint64
}
