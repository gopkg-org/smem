//go:build amd64 && linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package main

import (
	"os"
	"strconv"
	"strings"

	"github.com/olekukonko/tablewriter"
	"gopkg.org/size"
	"gopkg.org/smem"
)

func main() {
	processes, err := smem.New()
	if err != nil {
		panic(err)
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"PID", "User", "Command", "Swap", "VSS", "USS", "PSS", "RSS"})
	table.SetColumnAlignment([]int{
		tablewriter.ALIGN_LEFT,
		tablewriter.ALIGN_LEFT,
		tablewriter.ALIGN_LEFT,
		tablewriter.ALIGN_RIGHT,
		tablewriter.ALIGN_RIGHT,
		tablewriter.ALIGN_RIGHT,
		tablewriter.ALIGN_RIGHT,
		tablewriter.ALIGN_RIGHT,
	})

	for _, process := range processes {
		table.Append([]string{
			strconv.Itoa(process.PID),
			process.User,
			truncate(process.Command, 30),
			size.Size(process.Swap).SI(),
			size.Size(process.VSS).SI(),
			size.Size(process.USS).SI(),
			size.Size(process.PSS).SI(),
			size.Size(process.RSS).SI(),
		})
	}

	table.Render()
}

func truncate(s string, l int) string {
	if len(s) < l {
		return s
	}

	return s[:strings.LastIndex(s[:l], " ")]
}
