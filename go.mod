module gopkg.org/smem

go 1.21.1

require (
	github.com/olekukonko/tablewriter v0.0.5
	gopkg.org/proc v0.2.0
	gopkg.org/size v1.1.0
)

require (
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
